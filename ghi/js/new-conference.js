// console.log("hello - from new-location.js")
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        const option = document.createElement('option');
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
        }
        // console.log(selectTag);
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
    //   console.log('need to submit the form data');
      const formData = new FormData(formTag);
      // console.log(formData);
      const json = JSON.stringify(Object.fromEntries(formData));
      // console.log(json);
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
          method: "post",
          body: json,
          headers: {
              'Content-Type': 'application/json',
          },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
          // console.log(newConference);
      }
    });



  });
